import java.util.Scanner;

class Roulette{
    //method to check if user won
    public static boolean winCheck(int numberChosen, int numberWin){
        if (numberChosen == numberWin){
            System.out.println("user won!");
            return true;
        }
        else{
            System.out.println("user lost.");
            System.out.println("you chose "+ numberChosen + " and the winning number was "+ numberWin);
            return false;
        }
    }
    public static void main(String[] args){
        //create objects
        RouletteWheel wheel = new RouletteWheel();
        Scanner keyboard = new Scanner(System.in);

        //user values
        int userMoney = 1000;
        int moneyBet;
        int moneyLost = 0;
        int moneyWon = 0;

        //ask user for bet
        String betAnswer = "";
        int numberAnswer;

        while(userMoney >= 1){
            System.out.println("Would you like to bet?");
            betAnswer = keyboard.next();
            if (betAnswer.equals("yes")){
                //spin the wheel
                wheel.spin();
                moneyBet = userMoney + 1;

                numberAnswer = 37;
                while (numberAnswer >= 37){
                    System.out.println("Please enter a number:");
                    numberAnswer = keyboard.nextInt();
                }
                
                while (moneyBet > userMoney){
                    System.out.println("how much money would you like to bet?");
                    moneyBet = keyboard.nextInt();
                }
                //check if user won
                boolean win = winCheck(numberAnswer, wheel.getValue());
                if (win == true){
                    userMoney += 35 * moneyBet;
                    moneyWon += 35 * moneyBet;
                    System.out.println(userMoney);
                }
                else{
                    userMoney -= moneyBet;
                    moneyLost += moneyBet;
                    System.out.println(userMoney);
                }
            }
            else if (betAnswer.equals("no")){
                //print total amount lost/won
                System.out.println("\nyou won "+ moneyWon + " dollars and lost " + moneyLost + " dollars.");
                System.out.println("Au revoir!");
                break;
            }
    }
    if (userMoney == 0){
        System.out.println("out of money, goodbye!");
    }
    }
}