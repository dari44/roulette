import java.util.Random;

public class RouletteWheel {
    //inital values
    private Random random;
    private int value = 0;

    //constructor
    public RouletteWheel(){
        this.random = new Random();
    }

    //get methods
    public int getValue(){
        return this.value;
    }

    //custom methods
    public void spin(){
        this.value = random.nextInt(37); //excludes typed number
    }
}
